'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', 'LoginController.form').middleware('portal');
Route.get('/forbidden', 'LoginController.errors').as('errors');
Route.get('/dashboard', 'ContentController.dashboard').middleware('portal');
Route.get('/profile', 'ContentController.profileView').middleware('portal');
Route.get('/sense/analytic/analytic1', 'SenseController.analytic1').middleware('portal');
Route.post('/sense/analytic/analytic1', 'SenseController.analytic1').middleware('portal');
Route.get('/sense/analytic/analytic2', 'SenseController.analytic2').middleware('portal');
Route.post('/sense/analytic/analytic2', 'SenseController.analytic2').middleware('portal');
Route.get('/sense/analytic/analytic3', 'SenseController.analytic3').middleware('portal');
Route.post('/sense/analytic/analytic3', 'SenseController.analytic3').middleware('portal');
Route.get('/sense/analytic/analytic7', 'SenseController.analytic7').middleware('portal');
Route.post('/sense/analytic/analytic7', 'SenseController.analytic7').middleware('portal');
Route.get('/sense/analytic/analytic8', 'SenseController.analytic8').middleware('portal');
Route.post('/sense/analytic/analytic8', 'SenseController.analytic8').middleware('portal');
Route.get('/sense/analytic/mapping', 'SenseController.mapping').middleware('portal');
Route.post('/sense/analytic/mapping', 'SenseController.mapping').middleware('portal');
Route.get('/tone/analytic/sender/day', 'ToneController.senderDay').middleware('portal');
Route.post('/tone/analytic/sender/day', 'ToneController.senderDay').middleware('portal');
Route.get('/', 'ToneController.senderWeek').middleware('portal');
Route.post('/tone/analytic/sender/week', 'ToneController.senderWeek').middleware('portal');
Route.get('/perk/absence', 'PerkController.absence').middleware('portal');
Route.get('/perk/absence/user/:userToken', 'PerkController.detailAbsence').middleware('portal');
Route.post('/perk/absence/user/:userToken', 'PerkController.detailAbsence').middleware('portal');


Route.post('/', 'LoginController.form').as('signonsend');
Route.post('/api/user/check_user', 'api/UserController.checkUser').as('api_check_user');
Route.post('/api/mapping/getRealTime', 'api/ApiAjaxSenseController.getRealTime');
Route.post('/api/mapping/getDeviceRealTime', 'api/ApiAjaxSenseController.getDeviceRealTime');
Route.post('/api/mapping/getWeek', 'api/ApiAjaxSenseController.getWeek');
Route.post('/api/mapping/getFloor', 'api/ApiAjaxSenseController.getFloor');
Route.post('/api/mapping/mappingRealTimeDevices', 'api/ApiAjaxSenseController.mappingRealTimeDevices');
Route.post('/api/mapping/mappingRealTime', 'api/ApiAjaxSenseController.mappingRealTime');
Route.post('/api/login/user', 'api/UserController.login');

Route.get('/portal', 'LoginController.loginViaPortal').middleware('portal');
Route.get('/portal/profile/:token', 'ContentController.profileView').middleware('SSO');
Route.get('/portal/sonicsense/analytic/1/:token', 'SenseController.analytic1').middleware('SSO');
Route.post('/portal/sonicsense/analytic/1/:token', 'SenseController.analytic1').middleware('SSO');
Route.get('/portal/sonicsense/analytic/2/:token', 'SenseController.analytic2').middleware('SSO');
Route.post('/portal/sonicsense/analytic/2/:token', 'SenseController.analytic2').middleware('SSO');
Route.get('/portal/sonicsense/analytic/3/:token', 'SenseController.analytic3').middleware('SSO');
Route.post('/portal/sonicsense/analytic/3/:token', 'SenseController.analytic3').middleware('SSO');
Route.get('/portal/sonicsense/analytic/7/:token', 'SenseController.analytic7').middleware('SSO');
Route.post('/portal/sonicsense/analytic/7/:token', 'SenseController.analytic7').middleware('SSO');
Route.get('/portal/sonicsense/analytic/8/:token', 'SenseController.analytic8').middleware('SSO');
Route.post('/portal/sonicsense/analytic/8/:token', 'SenseController.analytic8').middleware('SSO');
Route.get('/portal/sonicsense/analytic/mapping/:token', 'SenseController.mapping').middleware('SSO');
Route.post('/portal/sonicsense/analytic/mapping/:token', 'SenseController.mapping').middleware('SSO');
Route.get('/portal/sonictone/analytic/sender/day/:token', 'ToneController.senderDay').middleware('SSO');
Route.post('/portal/sonictone/analytic/sender/day/:token', 'ToneController.senderDay').middleware('SSO');
Route.get('/portal/sonictone/analytic/sender/week/:token', 'ToneController.senderWeek').middleware('SSO');
Route.post('/portal/sonictone/analytic/sender/week/:token', 'ToneController.senderWeek').middleware('SSO');

Route.get('/cms/login', 'cms/LoginController.index').middleware('cmsLogin');
Route.post('/cms/login', 'cms/LoginController.index').middleware('cmsLogin');
Route.post('/api/cms/datatable/:table/:whereclause', 'cms/DatatableController.serverSide').middleware('cmsLogin');
Route.resource('/cms/device_user', 'cms/DeviceUserController').middleware('cmsLogin');
Route.post('/cms/device_user/:id', 'cms/DeviceUserController.update').middleware('cmsLogin');
Route.get('/cms/logout', 'cms/LoginController.logout').middleware('cmsLogin');