define({ "api": [
  {
    "type": "post",
    "url": "/api/user/check_user",
    "title": "User Authentication for get credentials",
    "name": "Authentication",
    "group": "1._User",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "key",
            "description": "<p>Partner key from Sonicboom.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user",
            "description": "<p>Username login in my.sonicboom.co.id.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ok",
            "description": "<p>Status response.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message response.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>List of route if request is valid.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"ok\": 200,\n  \"message\": \"Success Get/Update/Create.\",\n  \"data\": {\n             \"analytic_7\": \"MXBweXVjbjR0eHFpaHdnbnRsMGNwZm2Cd2loMzksWHNLNE82bD5SRl9WZFdbVmQzTDlnT3NNXHE7MVF0MXxScU9jV0VfdFZueDdPQz9C\",\n             \"mapping\": \"MXBweXVjbjR0eHFpaHdnbnRsMGNwZm2Cd2loM29hcXdqcGkxWHVNMlI1bTZPOFdTZDdbWGYxTzhoVm1TWFpINlF2M3pVcFRZVDdXdFZzeDlRQUJB\",\n          }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Partner or User not be found:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"ok\": 403,\n  \"message\": \"Partner or user not found.\"\n}",
          "type": "json"
        },
        {
          "title": "Update Error",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"ok\": 500,\n  \"message\": \"Error internal.\"\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "ok",
            "description": "<p>Status response.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message response.</p>"
          }
        ]
      }
    },
    "filename": "app/Controllers/Http/api/UserController.js",
    "groupTitle": "1._User"
  },
  {
    "type": "get",
    "url": "/portal/?token=:token",
    "title": "Implement feature Sonicboom in your iframe*",
    "name": "Set_in_your_iframe",
    "group": "2._IFRAME",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>token one of data above.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success showing feature:",
          "content": "HTTP/1.1 200 OK\n{\n  View page sonicboom feature.\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error showing feature:",
          "content": "HTTP/1.1 403 Not Found\n{\n  View error page\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Controllers/Http/LoginController.js",
    "groupTitle": "2._IFRAME"
  }
] });
