#!/bin/bash
# using mobassh
cd /cygdrive/c/Bitnami/wampstack-7.1.20-1/apache2/sandbox/nodeproject/sonicboom-portal
cmd.exe
set PATH=C:\Program Files\Git\bin;%PATH%
set PATH=C:\Bitnami\wampstack-7.1.20-1\php;%PATH%
set PATH=C:\ProgramData\ComposerSetup\bin;%PATH%
set PATH=C:\Program Files\nodejs;%PATH%
set PATH=C:\Users\Administrator\AppData\Roaming\npm;%PATH%
set COMPOSER_HOME="%USERPROFILE%/AppData/Roaming/Composer"
git checkout master
git pull origin master
npm install
pm2 restart sonicboom-portal
exit
