'use strict'

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class User extends Model {
  static get table() {
    return 'sb_user';
  }
  static get primaryKey() {
    return 'u_id';
  }
  static get hidden() {
    return ['u_password', 'u_salt', 'u_hash']
  }

  static get visible() {
    return ['u_username', 'u_token'];
  }
  partnerAdmin() {
    return this.hasMany('App/Models/PartnerAdmin', 'u_id', 'pa_user_id')
  }
  static boot() {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens() {
    return this.hasMany('App/Models/Token')
  }
}

module.exports = User
