'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Token extends Model {
    static get table() {
        return 'sb_user_sso_token';
      }
      static get primaryKey() {
        return 'sust_id';
      }
}

module.exports = Token
