'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class PartnerAdmin extends Model {
    static get table() {
        return 'sb_partner_admin';
    }
    static get primaryKey() {
        return 'pa_id';
    }
    Partner() {
        return this.hasOne('App/Models/Partner', 'pa_partner_id', 'p_id');
    }
}

module.exports = PartnerAdmin
