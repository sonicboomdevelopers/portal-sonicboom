'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class DeviceUser extends Model {
    static get table() {
        return 'sb_device_user';
    }
}

module.exports = DeviceUser
