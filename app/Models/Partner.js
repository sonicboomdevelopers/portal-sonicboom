'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Partner extends Model {
    static get table() {
        return 'sb_partner';
    }
    static get primaryKey() {
        return 'p_id';
    }
}

module.exports = Partner
