'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Database = use('Database');
const Component = use('./../Controllers/Http/ComponentController');
const User = use('./../Models/User');
const Env = use('Env');
class Login {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  constructor () {
      this.Component = new Component();
  }
  async handle ({ auth, request , response, view, session }, next) {
    let valid = false;
    let data = this.Component.decodeURLModular(request.originalUrl());
    if(data[1]){
      let checkUserValidToken = await Database.table('sb_user_sso_token').where('sust_token', data[1]).first();
      if(checkUserValidToken !== undefined){
        checkUserValidToken.sust_expired = this.Component.getTimeConverter(checkUserValidToken.sust_expired);
        if(this.Component.getTimeConverter(checkUserValidToken.sust_expired) >= this.Component.getDateTimeNow()){
        const query = User.query();
        query.innerJoin('sb_partner_admin', 'sb_partner_admin.pa_user_id', 'sb_user.u_id');
        query.innerJoin('sb_partner', 'sb_partner.p_id', 'sb_partner_admin.pa_partner_id');
        query.where({ u_is_active: 1, p_is_active: 1, u_id:   checkUserValidToken.sust_user_id });
        let userList = await query.fetch();
        try{
          if(auth.check()){
            let token = this.Component.encryptRedirectSSO(userList, data[0]);
            return response.redirect(data[0]+'/'+token);
          }
        }
        catch(error){
        }
        try{
          let identifierUser = await User.find(checkUserValidToken.sust_user_id);
          await auth.login(identifierUser);
          let token = this.Component.encryptRedirectSSO(userList, data[0]);
          return response.redirect(data[0]+'/'+token);
        }
        catch(error){
          console.log(error)
        }
      }
    }
  }
  if(!valid){
    let error_code = Env.get('ERROR_STATUS_FORBIDDEN');
    let error_title = 'Forbidden';
    let error_description = 'You\'r not have premission';
    return response.status(error_code).send(view.render('error', { error_code: error_code, error_title:error_title, error_description: error_description }))
  }
    await next()
  }
}

module.exports = Login
