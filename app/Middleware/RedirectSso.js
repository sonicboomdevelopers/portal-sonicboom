'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Component = use('./../Controllers/Http/ComponentController');
class RedirectSso {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  constructor () {
    this.Component = new Component();
}
  async handle ({ request , params }, next) {
    let explodeToken = this.Component.decryptRedirectSSO(params.token);
    for(var i in explodeToken){
      request[i] = explodeToken[i];
    }
    await next();
  }
}

module.exports = RedirectSso
