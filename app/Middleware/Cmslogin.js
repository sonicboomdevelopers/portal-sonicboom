'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class Cmslogin {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({session,  request, response }, next) {
    if(request.url() === '/cms/login'){
      if(session.get('is_access_cms')){
        return response.route('/cms/device_user');
      }
    }else{
      if(!session.get('is_access_cms')){
        return response.route('/cms/login');
      }
    };
    await next();
  }
}

module.exports = Cmslogin
