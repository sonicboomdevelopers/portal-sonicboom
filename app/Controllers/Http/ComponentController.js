'use strict'
const Env = use('Env');
const Encryption = use('Encryption')
const Database = use('Database');
const HASH_ALGORITHM = Env.get('HASH_ALGORITHM');
const jwt = require('jsonwebtoken');
const jwtKey = Env.get('APP_KEY');
const MomentTZ = require('moment-timezone');
const crypto = require('crypto');
const Encrypt = use('./EncryptController.js');
class ComponentController {
    constructor(){
        this.Encrypt = new Encrypt();
    }
    jwtAuth(header, res) {
        const token = header;
        let valid = true
        if (!token) return valid = false
        jwt.verify(token, jwtKey, function (err, decoded) {
            if (err) return valid = false
            valid = decoded.user_id
        })
        return valid
    }

    queryString(query) {
        let queryString = decodeURIComponent(query);
        let splitQueryString = queryString.split("?");
        let dataQueryString = new Array();
        splitQueryString = splitQueryString.slice(1, splitQueryString.length);
        for (let i = 0; i < splitQueryString.length; i++) {
            let splitArray = splitQueryString[i].split("=");
            dataQueryString[splitArray[0]] = splitArray[1];
        }
        return dataQueryString;
    }

    randomNumber(min, max) {
        return Math.round(Math.random() * (max - min) + min);
    }

    async getSonicSensePartner(request){
        let subDongleList = Database.from('sb_partner_has_dongle')
                                          .where('pd_partner', request)
                                          .select('pd_dongle')
        let dongleList = await Database.from('sb_dongle')
                                           .whereIn('do_id', subDongleList)
                                           .whereNotNull('do_name')
                                           .where('do_status', 1)
                                           .orderBy('do_name', 'ASC');
        return dongleList;
    }

    async getDeviceCountRealTime(dongle,step,size,start,end,distance){
        let gettingData
        if(start != null && end != null){
            gettingData = await Database.raw("select date_format(Date_add(step,interval 7 hour),'%a,%d - %H:%i') step , COALESCE(count, 0) as count from ( Select FROM_UNIXTIME(UNIX_TIMESTAMP('"+end+":59')-MOD(UNIX_TIMESTAMP('"+end+":59'),60)-((FLOOR((UNIX_TIMESTAMP('"+end+":59')-MOD(UNIX_TIMESTAMP('"+end+":59'),60)-`timestamp`-(3600*7))/(?*60))+1)*?*60)-(3600*7)) times,count(distinct mac) count from sb_wifiprobe where timestamp between UNIX_TIMESTAMP('"+start+":00') -(3600*7) and UNIX_TIMESTAMP('"+end+":59') -(3600*7) and probe = ? and distance <= ? group by times ) a right join ( select FROM_UNIXTIME(UNIX_TIMESTAMP('"+end+":00')-(?*?*60)+(id*?*60)-(3600*7)) step from sb_count_helper ORDER BY id asc limit ? ) b on a.times = b.step", [parseInt(step),parseInt(step), dongle, parseInt(distance), parseInt(size), parseInt(step), parseInt(step), parseInt(size)])
        }else{
            gettingData = await Database.raw("Select date_format(step,'%a,%d - %H:%i') step , COALESCE(count, 0) as count from ( Select FROM_UNIXTIME(UNIX_TIMESTAMP()-MOD(UNIX_TIMESTAMP(),60)-((FLOOR((UNIX_TIMESTAMP()-MOD(UNIX_TIMESTAMP(),60)-`timestamp`)/(?*60))+1)*?*60)+(3600*7)) times,count(distinct mac) count from sb_wifiprobe where timestamp > UNIX_TIMESTAMP()-MOD(UNIX_TIMESTAMP(),60) - (?*?*60) and probe = ? and distance <= ? group by times ) a right join ( select FROM_UNIXTIME(UNIX_TIMESTAMP()-(MOD(UNIX_TIMESTAMP(),60))-(?*?*60)+(id*?*60)+(3600*7)) step from sb_count_helper ORDER BY id asc limit ? ) b on a.times = b.step ", [parseInt(step), parseInt(step), parseInt(step), parseInt(size), dongle, distance, parseInt(size), parseInt(step), parseInt(step), parseInt(size)])
        }
        return gettingData[0];
    }

    async getDeviceRealTime(dongle,size,mac,start,end, distance){
        let gettingData
        if(start != null && end != null){
            gettingData = await Database.raw("Select mac, min(distance) distance from sb_wifiprobe  where timestamp+(3600*7) between UNIX_TIMESTAMP('?:00') and UNIX_TIMESTAMP('?:59') and distance < ? and mac like '%"+mac+"%' and probe = ? group by mac order by distance, mac Limit ?", [start, end, parseInt(distance), mac, dongle, parseInt(size)])
        }else{
            gettingData = await Database.raw("select mac, min(distance) distance from sb_wifiprobe where timestamp > UNIX_TIMESTAMP() - 3600 and distance < ? and mac like '%"+mac+"%' and probe = ? group by mac order by distance,mac Limit ?", [parseInt(distance), dongle, parseInt(size)])
        }
        return gettingData[0];
    }

    async getEstimationVisitor(dongle, date){
        let gettingData = await Database.raw("Select LEFT(Convert(Concat(id-1,'0000'),TIME),5) as hours, coalesce(b.count,0) count FROM `sb_count_helper` a left join ( Select count, date_format(from_unixtime(`timestamp`+(7*3600)),'%H') hours FROM `sb_hourly_wifiprobe` where timestamp between UNIX_TIMESTAMP('"+date+" 00:00:00') - (7*3600) and UNIX_TIMESTAMP('"+date+" 23:59:59') - (7*3600) and probe = ? group by hours ) b on id-1 = b.hours where a.id < 25 order by hours", [dongle])
        return gettingData[0];
    }
    
    async getTotalVisitor(dongle, date){
        let gettingData = await Database.raw("Select count(distinct mac) count from sb_hour_dist_wifiprobe where probe = ? and timestamp between UNIX_TIMESTAMP('"+date+" 00:00:00') - (7*3600) and UNIX_TIMESTAMP('"+date+" 23:59:59') - (7*3600)", [dongle])
        return gettingData[0];
    }   

    async getWeeklyAnalytic(dongle, start, end){
        let gettingData = await Database.raw("Select Date_Format(Date_add('"+start+"',INTERVAL id-1 DAY),'%a, %d %b') days, coalesce(count,0) count  from sb_count_helper a left join ( SELECT count, Date(FROM_UNIXTIME(`timestamp`)) timestamp  FROM sb_daily_wifiprobe  where `timestamp` between UNIX_TIMESTAMP('"+start+" 00:00:00') and UNIX_TIMESTAMP('"+end+" 23:59:59')  and probe = ?  GROUP BY timestamp ) b on  Date_add('"+start+"',INTERVAL id-1 DAY) = b.timestamp where id < 8", [dongle])
        return gettingData[0];
    }

    async getMonthlyAnalytic(dongle, year){
        let start = this.getDateConverter(new Date(year+'-01-01'))+' 00:00:00';
        let end = this.getDateConverter(new Date(year+'-12-31'))+' 23:59:59';
        let gettingData = await Database.raw("select DATE_FORMAT(DATE_ADD('"+start+"',  INTERVAL id-1 MONTH),'%b') months, coalesce(b.count,0) count from sb_count_helper a left join ( Select count, FROM_UNIXTIME(`timestamp`) timestamp FROM `sb_monthly_wifiprobe` where probe = ? and timestamp between UNIX_TIMESTAMP('"+start+"') AND UNIX_TIMESTAMP('"+end+"') group by `timestamp` ) b on DATE_ADD('"+start+"', INTERVAL id-1 MONTH) = b.timestamp WHERE id < 13", [dongle]);
        return gettingData[0];
    }

    async getDailySender(date, logCompany){
        let gettingData = await Database.raw("Select LEFT(Convert(Concat(id-1,'0000'),TIME),5) as hours, coalesce(b.count,0) count FROM `sb_count_helper` a left join ( SELECT count(DISTINCT log_created) count, HOUR(log_created) as hours, log_created, log_id, log_company from sb_log_sender WHERE MATCH(log_company) against('+"+logCompany[0].k_key+"*' in BOOLEAN MODE) and  DATE(log_created) = '"+date+"' GROUP BY(hours)) b on id-1 = b.hours where a.id < 25 ORDER BY hours", );
        return gettingData[0];
    }

    async getWeeklySender(start, end, logCompany){
        let gettingData = await Database.raw("Select Date_Format(Date_add('"+start+"',INTERVAL id-1 DAY),'%a, %d %b') days, coalesce(count,0) count from sb_count_helper a left join ( SELECT count(DISTINCT log_created) count, DATE(log_created) as dates, log_created, log_id, log_company from sb_log_sender WHERE log_created BETWEEN '"+start+"' AND '"+end+"' and MATCH(log_company) against('+"+logCompany+"*' in BOOLEAN MODE) GROUP BY dates ) b on Date_add('"+start+"',INTERVAL id-1 DAY) = b.dates LIMIT 0,7", );
        return gettingData[0];
    }

    async getBuildingByPartner(request){
        let subSubDongleList = Database.from('sb_partner_has_dongle')
                                        .where('pd_partner', request)
                                        .select('pd_dongle');
        let subDongleList = Database.from('sb_dongle')
                                    .distinct('building_id')
                                    .whereIn('do_id', subSubDongleList)
                                    .whereNotNull('building_id')
                                    .where('do_status', 1);
        let getBuilding = await Database.from('sb_ms_building')
                                           .whereIn('building_id', subDongleList)
                                           .orderBy('building_id', 'ASC')
                                           .select('building_id', 'building_name');
        return getBuilding;
    }

    async getMappingFloorByPartner(request){
        let subSubDongleList = Database.from('sb_partner_has_dongle')
                                        .where('pd_partner', request)
                                        .select('pd_dongle');
        let subDongleList = Database.from('sb_dongle')
                                    .whereIn('do_id', subSubDongleList)
                                    .select('floor_id');
        let flootList = await Database.from('sb_ms_floor')
                                        .whereIn('floor_id', subDongleList);
        return flootList;
    }

    async getDeviceLocation(id, time1 = null, time2 = null, floorId = null){
        let getLocation = Database.from('sb_mapping_device_activity');
        if(floorId != null){
            getLocation.where('mda_floor_id', floorId);
        }
        if(time1 != null){
            getLocation.where('mda_timestamp >=', time1);
            getLocation.where('mda_timestamp <=', time2);

        }
        getLocation.where('mda_device_id', id)
        getLocation.orderBy('mda_timestamp', 'ASC')
        return await getLocation;
    }

    async getDongleFloor(partnerId, buildingId){
        let FloorResult = await Database.from('sb_dongle')
                                        .innerJoin('sb_ms_building', 'sb_dongle.building_id', 'sb_ms_building.building_id')
                                        .innerJoin('sb_partner_has_dongle', 'sb_dongle.do_id', 'sb_partner_has_dongle.pd_dongle')
                                        .innerJoin('sb_ms_floor', 'sb_dongle.floor_id', 'sb_ms_floor.floor_id')
                                        .where('pd_partner', partnerId)
                                        .where('sb_dongle.building_id', buildingId)
                                        .groupBy('sb_ms_floor.floor_id')
                                        .select('sb_ms_floor.floor_id', 'sb_ms_floor.floor_name');
        return FloorResult;
    }

    async getMappingMsFloorByParameter(name, value){
        let FloorResult = await Database.from('sb_ms_floor')
                                        .where(name, value)
                                        .first();
                                        
        return FloorResult;
    }

    async getDongleMapping(floorId, startDate = null, endDate = null){
        let gettingData = await Database.raw("select coalesce(count,0) count,do_name,description,do_x,do_y,do_r from ( select count(distinct mac) count,probe from sb_wifiprobe w join ( select scanner_chip_id,do_limit_distance from sb_dongle where floor_id = ? and do_status = 1 ) d on w.probe = d.scanner_chip_id and w.distance <= d.do_limit_distance where timestamp between UNIX_TIMESTAMP()-300 and UNIX_TIMESTAMP() group by probe )   a right join (select do_name,description,do_x,do_y,scanner_chip_id,floor_scale*do_limit_distance as do_r  from sb_dongle sd join sb_ms_floor f on sd.floor_id = f.floor_id where sd.floor_id = ?  and sd.do_status = 1) b ON a.probe = b.scanner_chip_id order by count desc ",  [floorId, floorId]);
        return gettingData[0];
    }

    async getStore(){
        let storeList = await Database.from('sb_mapping_store')
                                        .rightJoin('sb_mapping_store_detail', 'sb_mapping_store_detail.msd_store_id', 'sb_mapping_store.mps_id')
        return storeList
    }

    async getDongleMapping(floorId){
        let gettingData = await Database.raw("select coalesce(count,0) count,do_name,description,do_x,do_y,do_r from ( select count(distinct mac) count,probe from sb_wifiprobe w join ( select scanner_chip_id,do_limit_distance from sb_dongle where floor_id = "+floorId+" and do_status = 1 ) d on w.probe = d.scanner_chip_id and w.distance <= d.do_limit_distance where timestamp between UNIX_TIMESTAMP()-300 and UNIX_TIMESTAMP() group by probe ) a right join (select do_name,description,do_x,do_y,scanner_chip_id,floor_scale*do_limit_distance as do_r  from sb_dongle sd join sb_ms_floor f on sd.floor_id = f.floor_id where sd.floor_id = "+floorId+"  and sd.do_status = 1) b ON a.probe = b.scanner_chip_id order by count desc",);
        return gettingData[0];
    }

    async getDongleMappingDevice(floorId, startDate, endDate){
        let gettingData = await Database.raw("select count(distinct mac) count from sb_wifiprobe w join ( select scanner_chip_id,do_limit_distance from sb_dongle where floor_id = ? and do_status = 1 ) d on w.probe = d.scanner_chip_id and w.distance <= d.do_limit_distance where timestamp between UNIX_TIMESTAMP()-300 and UNIX_TIMESTAMP()",  [floorId]);
        return gettingData[0];
    }

    hash(str) {
        var sha512 = crypto.createHash(HASH_ALGORITHM).update(str).digest('hex');
        return sha512;
    }

    jwtGenerate(user_id) {
        let newGenerateToken = jwt.sign({ user_id }, jwtKey, { expiresIn: '3d' })
        return newGenerateToken
    }

    splitData(variable) {
        var query = {};
        var pairs = (variable[0] === '?' ? variable.substr(1) : variable).split('&');
        for (var i = 0; i < pairs.length; i++) {
            var pair = pairs[i].split('=');
            query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
        }
        return query;
    }

    getDateTimeNow() {
        let today = new Date();
        let getDateNow = today.getFullYear() + '-' + String(today.getMonth() + 1).padStart(2, '0') + '-' + String(today.getDate()).padStart(2, '0') + ' ' + String(today.getHours()).padStart(2, '0') + ':' + String(today.getMinutes()).padStart(2, '0') + ':' + String(today.getSeconds()).padStart(2, '0');
        return MomentTZ().tz('Asia/Jakarta').format(getDateNow);
    }

    getDateTimeStart() {
        let today = new Date();
        let getDateNow = today.getFullYear() + '-' + String(today.getMonth() + 1).padStart(2, '0') + '-' + String(today.getDate()).padStart(2, '0') + ' 00:00:00';
        return MomentTZ().tz('Asia/Jakarta').format(getDateNow);
    }

    getSenseAnalytic(param) {
        let today = new Date();
        let getDateNow = ''
        if(param === 'start'){
            getDateNow = today.getFullYear() + '/' + String(today.getMonth() + 1).padStart(2, '0') + '/' + String(today.getDate()).padStart(2, '0') + ' 00:00:00';
        }else{
            getDateNow = today.getFullYear() + '/' + String(today.getMonth() + 1).padStart(2, '0') + '/' + String(today.getDate()).padStart(2, '0') + ' ' + String(today.getHours()).padStart(2, '0') + ':' + String(today.getMinutes()).padStart(2, '0') + ':' + String(today.getSeconds()).padStart(2, '0');
        }
        return MomentTZ().tz('Asia/Jakarta').format(getDateNow);
    }

    getDateDaysCustomeFromCurrent(days) {
        let today = new Date();
        today.setDate(today.getDate() + days);
        let getDateNow = today.getFullYear() + '-' + String(today.getMonth() + 1).padStart(2, '0') + '-' + String(today.getDate()).padStart(2, '0') + ' ' + String(today.getHours()).padStart(2, '0') + ':' + String(today.getMinutes()).padStart(2, '0') + ':' + String(today.getSeconds()).padStart(2, '0');
        return MomentTZ().tz('Asia/Jakarta').format(getDateNow);
    }

    getDateTimeEnd() {
        let today = new Date();
        let getDateNow = today.getFullYear() + '-' + String(today.getMonth() + 1).padStart(2, '0') + '-' + String(today.getDate()).padStart(2, '0') + ' 23:59:59';
        return MomentTZ().tz('Asia/Jakarta').format(getDateNow);
    }

    getDateHEvent() {
        let today = new Date();
        let getDateNow = today.getFullYear() + '-' + 11 + '-' + 24 + ' 23:59:59';
        return MomentTZ().tz('Asia/Jakarta').format(getDateNow);
    }

    getSSOUserTimes(){
        let today = new Date();
        today.setHours(today.getHours()+2);
        let getDateNow = today.getFullYear() + '-' + String(today.getMonth() + 1).padStart(2, '0') + '-' + String(today.getDate()).padStart(2, '0') + ' ' + String(today.getHours()).padStart(2, '0') + ':' + String(today.getMinutes()).padStart(2, '0') + ':' + String(today.getSeconds()).padStart(2, '0');
        return MomentTZ().tz('Asia/Jakarta').format(getDateNow);
    }

    getCountDateOfMonth(month = false, year = false){
        let date
        if(year !== false && month !== false){
            date = new Date(year+'-'+month); 
        }else{
            date = new Date() 
        }
        let y = date.getFullYear(), m = date.getMonth();
        return MomentTZ(new Date(y, m + 1, 0)).format('DD');
    }

    getTimeConverter(datetime) {
        return MomentTZ(datetime).format('YYYY-MM-DD HH:MM:ss');
    }

    getDateConverter(datetime) {
        return MomentTZ(datetime).format('YYYY-MM-DD');
    }

    getFirstMondayOfMonth(month = false){
        let findMonday
        if(month == false){
            findMonday = new Date();
        }else{
            findMonday = new Date(month);
        }
        findMonday = findMonday.getFullYear()+'-'+("0" + (findMonday.getMonth() + 1)).slice(-2)+'-01';
        let d = new Date(findMonday);
        var day = d.getDay(),
            diff = d.getDate() - day + (day == 0 ? -6:1);
        return this.getDateConverter(new Date(d.setDate(diff)));
    }

    getAbsenceCalendar(month = false, year = false){
        let monthPrev 
        if(month == 1){
            monthPrev = 12;
        }else{
            monthPrev = month-1;
        }
        let firstWeek = this.getFirstSundayOfMonth(year+'-'+month, true);
        if(firstWeek !== 1){
            let getLastDate =  this.getCountDateOfMonth(monthPrev, year);
            let data = []
            for(let i = firstWeek; i <= getLastDate; i++){
                data.push(i);
            }
            return data;
        }else{
            return false;
        }
    }

    getFirstSundayOfMonth(month = false, getDate = false){
        let findMonday
        if(month == false){
            findMonday = new Date();
        }else{
            findMonday = new Date(month);
        }
        findMonday = findMonday.getFullYear()+'-'+("0" + (findMonday.getMonth() + 1)).slice(-2)+'-01';
        let d = new Date(findMonday);
        var day = d.getDay(),diff = d.getDate() - day;
        if(getDate){
            let lastDate = new Date(d.setDate(diff));
            return lastDate.getDate();
        }else{
            return this.getDateConverter(new Date(d.setDate(diff)));
        }
    }
    getLastSaturdayOfMonth(month = false, getDate = false){
        let findMonday
        if(month == false){
            findMonday = new Date();
        }else{
            findMonday = new Date(month);
        }
        var y = findMonday.getFullYear(), m = findMonday.getMonth();
        findMonday = findMonday.getFullYear()+'-'+("0" + (findMonday.getMonth() + 1)).slice(-2)+'-'+MomentTZ(new Date(y, m + 1, 0)).format('DD');
        let d = new Date(findMonday);
        var day = d.getDay(),
            diff = d.getDate() - day + (day == 7 ? 0:6);
        if(getDate){
            let lastDate = new Date(d.setDate(diff))
            return lastDate.getDate();
        }else{
            return this.getDateConverter(new Date(d.setDate(diff)));
        }
    }

    getDateFormat1(date, monthYear){
        var monthNames = [
            "Jan", "Feb", "Mar",
            "Apr", "May", "Jun", "Jul",
            "Aug", "Sep", "Oct",
            "Nov", "Dec"
          ];
        
        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();
        if(monthYear){
            return monthNames[monthIndex] + ' ' + year;
        }else{
            return day + ' ' + monthNames[monthIndex] + ' ' + year;
        }
    }

    getDateFormat2(){
        let today = new Date();
        let getDateNow = String(today.getMonth() + 1).padStart(2, '0') + '/' + String(today.getDate()).padStart(2, '0') + '/' + today.getFullYear() + ' 00:00:00'; 
        return MomentTZ().tz('Asia/Jakarta').format(getDateNow);
    }

    getDateFormat3(){
        let today = new Date();
        let getDateNow = String(today.getMonth() + 1).padStart(2, '0') + '/' + String(today.getDate()).padStart(2, '0') + '/' + today.getFullYear() + ' '+ String(today.getHours()).padStart(2, '0') + ':' + String(today.getMinutes()).padStart(2, '0') + ':' + String(today.getSeconds()).padStart(2, '0');; 
        return MomentTZ().tz('Asia/Jakarta').format(getDateNow);
    }

    encryptRedirectSSO(user, route){
        let key = '{';
        if(route === '/portal/profile'){
            key += '"partner_id":"'+user.rows[0].pa_partner_id+'"';
        }else if(route === '/portal/sonicsense/analytic/7'){
            key += '"partner_id":"'+user.rows[0].pa_partner_id+'"';
        }else if(route === '/portal/sonicsense/analytic/8'){
            key += '"partner_id":"'+user.rows[0].pa_partner_id+'"';
        }else if(route === '/portal/sonicsense/analytic/1'){
            key += '"partner_id":"'+user.rows[0].pa_partner_id+'"';
        }else if(route === '/portal/sonicsense/analytic/mapping'){
            key += '"partner_id":"'+user.rows[0].pa_partner_id+'"';
        }
        key += '}';
        return encodeURIComponent(this.Encrypt.get_encrypt(key, Env.get('KEY_MODULAR_FEATURE')));
    }

    decryptRedirectSSO(token){
        let decrypt = decodeURIComponent(token);
        let getData = this.Encrypt.decrypt(decrypt, Env.get('KEY_MODULAR_FEATURE'));
        return JSON.parse(getData);
    }

    decodeURLModular(token){
        let getToken = token.split('?');
        try{
            getToken = getToken[1].split('token=');
            let decode = decodeURIComponent(getToken[1]);
            let getModularDestination = this.Encrypt.decrypt(decode, Env.get('KEY_MODULAR_FEATURE'));
            return getModularDestination;
        }
        catch(error){
            return false;
        }
    }

}

module.exports = ComponentController
