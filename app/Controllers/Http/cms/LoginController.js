'use strict'
const Database = use('Database');
const Env = use('Env');
const APP_URL = Env.get('APP_API_URL');
const Component = use('./../ComponentController');
const User = use('App/Models/User');
class LoginController {
    constructor() {
        this.Component = new Component();
    }
    async index({ session, auth, request, response, view }) {
        if (request.post()._csrf) {
            const data = request.post();
            const username = data.username;
            const password = data.password;
            const query = User.query();
            query.innerJoin('sb_partner_admin', 'sb_partner_admin.pa_user_id', 'sb_user.u_id');
            query.innerJoin('sb_partner', 'sb_partner.p_id', 'sb_partner_admin.pa_partner_id');
            query.where({ u_is_active: 1, p_is_active: 1, u_username: username });
            let userList = await query.fetch();
            if(userList.rows[0] !== undefined){
                let validUser = false;
                if(userList.rows[0].u_password ==  this.Component.hash(userList.rows[0].u_salt + password)){
                    validUser = true
                    if(validUser){
                        session.put('user_partner_cms', userList.rows[0].pa_partner_id)
                        session.put('is_access_cms', true);
                        session.put('user_full_name', userList.rows[0].u_fullname);
                        return response.redirect('/cms/device_user');
                    }else{
                        return view.render('cms.login', { warning: true, username });
                    }
                }else{
                    return view.render('cms.login', { warning: true, username });
                }
            }else{
                return view.render('cms.login', { warning: true, username });
            }

        }else{
            return view.render('cms.login', { APP_URL: APP_URL });
        }
    }
    async logout({session,  auth, request, response }) {
        session.forget('user_partner_cms');
        session.forget('is_access_cms');
        session.forget('user_full_name');
        return response.route('/cms/login');
    }
}

module.exports = LoginController
