'use strict'
const Database = use('Database');
const Env = use('Env')
const assetsUrl = Env.get('APP_ASSETS_UPLOAD')
const Component = use('./../ComponentController');
const DeviceUser = use('App/Models/cms/DeviceUser');
class DatatableController {
    constructor() {
        this.Component = new Component();
        this.columns = new Array();
    }
    async serverSide({ request, response, params }) {
        const keyReference = params.table;
        const paramsPost = request.post();
        let data = new Array();
        let query, queryFilter, queryAllData
        if(keyReference === 'device_user'){
            this.columns.push('du_id', 'du_device_id','du_mac', 'du_device_name');
            query =  DeviceUser.query();
            queryFilter = DeviceUser.query();
            queryAllData = await queryFilter.getCount();
            this.makeTable(paramsPost, query, keyReference);
            this.makeFiltered(paramsPost, queryFilter);
            queryFilter = await queryFilter.getCount();;
            let dataDeviceUser = await query.fetch();
            dataDeviceUser = await dataDeviceUser.rows;
            for (let i = 0; i < dataDeviceUser.length; i++) {
                let subData = new Array();
                let status, apps;
                subData.push("<label class='mt-checkbox mt-checkbox-single mt-checkbox-outline'><input type='checkbox' class='checkboxes' value='"+dataDeviceUser[i]['du_id']+"' /><span></span></label>");
                subData.push('<a href="/cms/device_user/'+dataDeviceUser[i]['du_id']+'" type="button" class="btn yellow mt-ladda-btn ladda-button btn-outline" data-style="contract" data-spinner-color="#333">Edit</span></a>');
                subData.push(dataDeviceUser[i]['du_device_id']);
                subData.push(dataDeviceUser[i]['du_mac']);
                subData.push(dataDeviceUser[i]['du_device_name']);
                data.push(subData);
            }
        }
        paramsPost.data = data
        response.send({ draw: paramsPost.draw, recordsTotal: queryAllData, recordsFiltered: queryFilter, data: data });
    }
    makeWhere(query, whereForWhat){
    }
    makeFilter(paramsPost, query){
        if(typeof paramsPost.search['value'] !== undefined){
            let search = paramsPost.search['value'];
            let like = '';
            for (let i = 0; i < this.columns.length; i++) {
                query.orWhere(this.columns[i], 'LIKE', '%' + search + '%')
            }
            this.makeOrder(paramsPost, query);
        }
    }

    makeOrder(paramsPost, query){
        if(typeof paramsPost.order !== undefined){
            query.orderBy(this.columns[parseInt(paramsPost.order[0]['column']) - 1 ], paramsPost.order[0]['dir']);
        }else{
            query.orderBy(this.columns[0], 'DESC');
        }
    }

    makeQuery(query){
        if(this.columns.length > 0 ){
            query.select(this.columns);
        }else{
            query.select('*');
        }
    }

    makeTable(paramsPost, query, whereClause){
        this.makeQuery(query);
        this.makeWhere(query, whereClause)
        this.makeFilter(paramsPost, query);
        if(paramsPost.length != -1){
            query.offset(parseInt(paramsPost.start));
            query.limit(parseInt(paramsPost.length));
        }
    }

    makeFiltered(paramsPost, query, whereClause){
        this.makeQuery(query);
        this.makeWhere(query, whereClause)
        this.makeFilter(paramsPost, query);
    }
}

module.exports = DatatableController