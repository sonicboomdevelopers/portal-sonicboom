'use strict'
const Database = use('Database');
const Env = use('Env')
const APP_URL = Env.get('API_URL')
const Component = use('./../ComponentController');
class DeviceUserController {
  constructor() {
    this.Component = new Component();
  }
  async index({ auth, request, response, view, session }) {
    let USER_FULL_NAME = session.get('user_full_name')
    return view.render('cms.content.tracking.index', { APP_URL: APP_URL, USER_FULL_NAME  })
  }
  async create({ session, request, response, view }) {
    let USER_FULL_NAME = session.get('user_full_name')
    return view.render('cms.content.tracking.form', { APP_URL: APP_URL, USER_FULL_NAME })
  }

  async store({ session, request, response, view }) {
    let data = request.post();
    const getDeviceHasInput = await Database.from('sb_device_user')
                                            .where('du_device_id', data.device_id)
                                            .first();
    if(getDeviceHasInput !== undefined){
        let Saving = await Database.from('sb_device_user')
                                    .insert([{
                                        du_device_id: data.device_id,
                                        du_mac: data.mac_address,
                                        du_device_name: data.device_name,
                                        du_partner_id: session.get('user_partner_cms'),
                                        created_at: this.Component.getDateTimeNow()
                                    }])
        if (Saving) {
            return response.redirect('/cms/device_user');
        } else {
            return response.redirect('/cms/device_user');
        }
    }else{
        let USER_FULL_NAME = session.get('user_full_name');
        let warning = true;
        return view.render('cms.content.tracking.form', { APP_URL: APP_URL, USER_FULL_NAME, warning })
    }
    
  }

  async update({ session, request, response, view }) {
    let data = request.post();
    let dataId = request.params.id;
    const getDeviceHasInput = await Database.from('sb_device_user')
                                            .where('du_device_id', data.device_id)
                                            .first();
    if(getDeviceHasInput === undefined){
        let Saving = await Database.table('sb_device_user')
                                    .where('du_id', dataId)
                                    .update({
                                            du_device_id: data.device_id,
                                            du_mac: data.mac_address,
                                            du_device_name: data.device_name,
                                            updated_at: this.Component.getDateTimeNow(),
                                        })

        if (Saving) {
        return response.redirect('/cms/device_user');
        } else {
        return response.redirect('/cms/device_user');
        }
    }else{
        let USER_FULL_NAME = session.get('user_full_name')
        let warning = true;
        const deviceId = data.device_id;
        const macAddress = data.mac_address;
        const deviceName = data.device_name;
        return view.render('cms.content.tracking.view', { APP_URL: APP_URL, USER_FULL_NAME, warning, id: dataId, deviceId, macAddress, deviceName})
    }
  }

  async show({ session, request, response, view }){
    const id = request.params.id;
    const detailGuideHelp = await Database.from('sb_device_user')
                                          .where('du_id', id)
                                          .first();
    const deviceId = detailGuideHelp.du_device_id;
    const macAddress = detailGuideHelp.du_mac;
    const deviceName = detailGuideHelp.du_device_name;
    let USER_FULL_NAME = session.get('user_full_name')
    return view.render('cms.content.tracking.view', { id, deviceId, macAddress, deviceName, USER_FULL_NAME})

  }
}

module.exports = DeviceUserController
