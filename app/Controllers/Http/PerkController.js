'use strict'
const Database = use('Database');
const Hash = use('Hash');
const Env = use('Env');
const APP_LIBRARY_UPLOAD = Env.get('APP_LIBRARY_UPLOAD');
const APP_ASSETS = Env.get('APP_ASSETS');
const Component = use('./ComponentController');
const Encrypt = use('./EncryptController.js');
const User = use('App/Models/User');
class PerkController {
    constructor() {
        this.Component = new Component();
        this.Encrypt = new Encrypt();
    }
    async absence({ session, view }){
        const getDeviceHasInput = await Database.select('du_device_name', 'du_device_id')
                                                .from('sb_device_user')
                                                .where('du_partner_id', session.get('user_partner'));
        return view.render('content.perk.index_absence', { listEmployment: getDeviceHasInput});
    }

    async detailAbsence({ session, view, response, request, params }){
        const getDeviceHasInput = await Database.select('du_device_name')
                                                .from('sb_device_user')
                                                .where('du_partner_id', session.get('user_partner'));
        let dataAbsence = [];
        let month
        let year
        let deviceId = params.userToken;
        if (request.post()._csrf) {
            let dataRequest = request.post();
            month = dataRequest.month;
            year = dataRequest.year;
        }else{
            let dateNow = new Date();
            month = dateNow.getMonth()+1;
            year = dateNow.getFullYear();
        }
        let titleAbsence = this.Component.getDateFormat1(new Date(year+'-'+month+'-1'), true);
        let countDate = this.Component.getCountDateOfMonth(month, year);
        let dateOneMonth = [];
        for (let i = 1; i <= countDate; i++) {
            dateOneMonth.push(new Date(year+'-'+month+'-'+i).getTime() / 1000);
        }
        const getAbsenceEmploye = await Database.from('sb_absence_list')
                                                .whereIn('abs_times', dateOneMonth)
                                                .where('abs_device_id', deviceId)
                                                .orderBy('abs_times', 'ASC');
        let dataPrev = this.Component.getAbsenceCalendar(month, year);
        if(dataPrev.length > 0){
            let temp = 0;
            for(let i = dataPrev[0]; i <= dataPrev[dataPrev.length -1]; i++){
                dataAbsence.push({ date: dataPrev[temp] })
                temp++ 
            }
        }
        for (let i = 0; i < countDate; i++) {
            let absence 
            if(getAbsenceEmploye[i]){
                
                let employeAbsence;
                if(getAbsenceEmploye[i].abs_status === 0){
                    employeAbsence =  1;
                }else if(getAbsenceEmploye[i].abs_status === 1){
                    employeAbsence =  2;
                }else if(getAbsenceEmploye[i].abs_status === 2){
                    employeAbsence =  3;
                }
                absence = { date: i+1, get: employeAbsence, checkIn: getAbsenceEmploye[i].abs_check_in, checkOut: getAbsenceEmploye[i].abs_check_out };
            }else{
                absence = { date: i+1 };
            }
            dataAbsence.push(absence);
        }
        let getLastSaturday = this.Component.getLastSaturdayOfMonth(year+'-'+month, true);
        if(getLastSaturday > 0){
            for(let i = 0; i < getLastSaturday; i++){
                dataAbsence.push({ date: i+1 })
            }
        }
        return view.render('content.perk.detail_absence', { weekOne: dataAbsence.splice(0,7), 
                                                            weekTwo : dataAbsence.splice(0,7), 
                                                            weekThree: dataAbsence.splice(0,7), 
                                                            weekFour: dataAbsence.splice(0,7), 
                                                            weekFive: dataAbsence.splice(0,7),
                                                            titleAbsence,
                                                            month,
                                                            year
                                                         });
    }
}

module.exports = PerkController
