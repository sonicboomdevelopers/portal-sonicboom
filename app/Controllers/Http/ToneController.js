'use strict'
const Database = use('Database');
const Hash = use('Hash');
const Env = use('Env');
const APP_LIBRARY_UPLOAD = Env.get('APP_LIBRARY_UPLOAD');
const APP_ASSETS = Env.get('APP_ASSETS');
const API_URL = Env.get('API_URL');
const Component = use('./ComponentController');
const Encrypt = use('./EncryptController.js');
const User = use('App/Models/User');
class ToneController {
    constructor() {
        this.Component = new Component();
        this.Encrypt = new Encrypt();
    }
    async senderDay({ session, view, request, response }){
        let starttime = false;
        let partner = false;
        let partnerId = false;
        let dataMapping = false;
        let getDailySender = false;
        let labelDate = false
        if (request.post()._csrf) {
            let data = request.post();
            partnerId = session.get('user_partner');
            partner = await Database.from('sb_partner_key')
                                          .where('k_partner', partnerId)
                                          .select('k_key');
            starttime = data.timetracking;
            labelDate = this.Component.getDateFormat1(new Date(starttime));
            getDailySender = await this.Component.getDailySender(starttime, partner);
            dataMapping = JSON.stringify({sender_days: getDailySender})
            return view.render('content.tone.analytic.sender.dailyreport',{ starttime, partner, partnerId, getDailySender, dataMapping, labelDate });
        }else{
            return view.render('content.tone.analytic.sender.dailyreport',{ starttime, partner, partnerId, getDailySender, dataMapping, labelDate });
        }
    }

    async senderWeek({ session, view, request, response }){
        let weekId = false;
        let dataMapping = false;
        let labelDate = false;
        let partner = false;
        let inpWeek = new Array();
        let date = new Date();
        let inpMonth = date.getFullYear()+'-'+(date.getMonth()+1)
        let monday = this.Component.getFirstMondayOfMonth(); // Get date first monday of month
        for(let i=0; i < 5; i++){
            let somedate = new Date(String(monday));
            inpWeek.push({ key :  monday+'|'+ this.Component.getDateConverter(this.addDays(somedate, 6)), value: this.Component.getDateFormat1(new Date(monday))+' - '+this.Component.getDateFormat1(new Date( this.Component.getDateConverter(this.addDays(somedate, 6))))})
            monday = this.Component.getDateConverter(this.addDays(monday, 7))
        }
        if (request.post()._csrf) {
            let data = request.post();
            inpMonth = data.timetracking;
            weekId = data.week;
            let range = weekId.split('|');
            partner = await Database.select('k_key')
                                    .from('sb_partner_key')
                                    .where('k_partner', session.get('user_partner')).first();
            let getWeeklySender = await this.Component.getWeeklySender(range[0], range[1], partner.k_key);
            dataMapping = JSON.stringify({get_weekly_sender: getWeeklySender})
            inpWeek = new Array();
            monday = this.Component.getFirstMondayOfMonth(inpMonth); // Get date first monday of month
            for(let i=0; i < 5; i++){
                let somedate = new Date(String(monday));
                inpWeek.push({ key :  monday+'|'+ this.Component.getDateConverter(this.addDays(somedate, 6)), value: this.Component.getDateFormat1(new Date(monday))+' - '+this.Component.getDateFormat1(new Date( this.Component.getDateConverter(this.addDays(somedate, 6))))})
                monday = this.Component.getDateConverter(this.addDays(monday, 7))
            }
            return view.render('content.tone.analytic.sender.weekreport',{ weekId, dataMapping, labelDate, inpWeek, inpMonth, monday, API_URL });
        }else{
            return view.render('content.tone.analytic.sender.weekreport',{ weekId, dataMapping, labelDate, inpWeek, inpMonth, monday, API_URL });
        }
    }

    getMonday(d) {
        d = new Date(d);
        var day = d.getDay(),
            diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
        return new Date(d.setDate(diff));
    }

    addDays(dates, days){    
        var date = new Date(dates);
        date.setDate(date.getDate() + days);
        return date;
    }
    
}

module.exports = ToneController
