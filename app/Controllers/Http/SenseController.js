'use strict'
const Database = use('Database');
const Hash = use('Hash');
const Env = use('Env');
const APP_LIBRARY_UPLOAD = Env.get('APP_LIBRARY_UPLOAD');
const APP_ASSETS = Env.get('APP_ASSETS');
const APP_LIBRARY_MAPS_UPLOAD = Env.get('APP_LIBRARY_MAPS_UPLOAD');
const Component = use('./ComponentController');
const Encrypt = use('./EncryptController.js');
const User = use('App/Models/User');
const API_URL = Env.get('API_URL');
class ContentController {
    constructor() {
        this.Component = new Component();
        this.Encrypt = new Encrypt();
    }
    async dashboard({ session, auth, request, response, view }){
        //await auth.logout();
        //let UserGetting = await User.query(1);

        //let listUser = await UserGetting.partnerAdmin().fetch();
        // const query = User.query();
        // query.innerJoin('sb_partner_admin', 'sb_partner_admin.pa_user_id', 'sb_user.u_id');
        // query.innerJoin('sb_partner', 'sb_partner.p_id', 'sb_partner_admin.pa_partner_id');
        // query.where({ u_is_active: 1, p_is_active: 1, u_username: 'sonicboom' });
        // let userList = await query.fetch();
        
        return view.render('content.dashboard',{ });
    }

    async mapping({ session, view, request, response }){
        let dateTime = this.Component.getDateFormat2();
        let dateTimeEnd = this.Component.getDateFormat3();
        let floorId = null;
        let device = null;
        let dataMapping = new Array();
        let pathImage = null;
        let imgW = 1000;
        let imgH = 800;
        let chooseFloor = null;
        let chooseBuilding = null;
        let buildingList = await this.Component.getBuildingByPartner(request.partner_id);
        if (request.post()._csrf) {
            let data = request.post();
            dateTime = ':00';
            dateTimeEnd = ':00';
            floorId = data.floor;
            let floorList = await this.Component.getMappingFloorByPartner(request.partner_id);
            let row = await this.Component.getMappingMsFloorByParameter('floor_id', floorId);
            pathImage = row.floor_path_image;
            imgW = row.img_width;
            imgH = row.img_height;
            chooseFloor = data.floor;
            chooseBuilding = data.build;
            dataMapping = JSON.stringify({ dongle : await this.Component.getDongleMapping(floorId) })
            return view.render('content.sense.analytic.mapping',{ dateTime, dateTimeEnd, floorId, device, dataMapping, pathImage, imgW, imgH, chooseFloor, chooseBuilding, buildingList, floorList, API_URL, APP_LIBRARY_MAPS_UPLOAD });

        }else{
            let itemList = await this.Component.getDeviceLocation(null);
            let storeList = await this.Component.getStore();
            let floorList = await this.Component.getMappingFloorByPartner(request.partner_id);
            dataMapping = JSON.stringify({ store: storeList, activity: itemList });
            return view.render('content.sense.analytic.mapping',{ dateTime, dateTimeEnd, floorId, device, dataMapping, pathImage, imgW, imgH, chooseFloor, chooseBuilding, buildingList,itemList , storeList, floorList, API_URL, APP_LIBRARY_MAPS_UPLOAD });
        }
        
    }

    async analytic1({ session, view, request, response }){
        let dongleList = await this.Component.getSonicSensePartner(request.partner_id);
        let dongleId = null;
        let starttime = false;
        let dataMapping = new Array();
        let ssAnalyticEst = null;
        let summary = null;
        if (request.post()._csrf) {
            let data = request.post();
            dongleId = data.build;
            starttime = data.timetracking;
            ssAnalyticEst = await this.Component.getEstimationVisitor(dongleId, starttime);
            summary = await this.Component.getTotalVisitor(dongleId, starttime);
            // dataMapping['estimation'] = ssAnalyticEst;
            // dataMapping['summary'] = summary;
            dataMapping = JSON.stringify({estimation: ssAnalyticEst, summary: summary})
            //console.log(JSON.stringify(dataMapping));
            return view.render('content.sense.analytic.estimationvisitoranalytic',{ dongleList, dongleId ,starttime ,dataMapping ,ssAnalyticEst ,summary });
        }else{
            return view.render('content.sense.analytic.estimationvisitoranalytic',{ dongleList, dongleId ,starttime ,dataMapping ,ssAnalyticEst ,summary });
        }
    }

    async analytic2({ session, view, request, response }){
        let dongleList = await this.Component.getSonicSensePartner(request.partner_id);
        let dongleId = null;
        let weekId = false;
        let dataMapping = new Array();
        let inpWeek = new Array();
        let date = new Date();
        let weekData = new Array();
        let monday = this.Component.getFirstMondayOfMonth(); // Get date first monday of month
        //console.log(somedate)
        //console.log(this.Component.getDateFormat1(new Date(monday)));
        for(let i=0; i < 5; i++){
            let somedate = new Date(String(monday));
            inpWeek.push({ key :  monday+'|'+ this.Component.getDateConverter(this.addDays(somedate, 6)), value: this.Component.getDateFormat1(new Date(monday))+' - '+this.Component.getDateFormat1(new Date( this.Component.getDateConverter(this.addDays(somedate, 6))))})
            monday = this.Component.getDateConverter(this.addDays(monday, 7))
        }
        let inpMonth = date.getFullYear()+'-'+(date.getMonth()+1)
        if (request.post()._csrf) {
            let data = request.post();
            dongleId = data.build;
            inpMonth = data.timetracking;
            weekId = data.week;
            //console.log(weekId)
            let range = weekId.split('|');
            dataMapping = JSON.stringify({analytic: await this.Component.getWeeklyAnalytic(dongleId, range[0], range[1])});
            inpWeek = new Array();
            monday = this.Component.getFirstMondayOfMonth(inpMonth); // Get date first monday of month
            for(let i=0; i < 5; i++){
                let somedate = new Date(String(monday));
                inpWeek.push({ key :  monday+'|'+ this.Component.getDateConverter(this.addDays(somedate, 6)), value: this.Component.getDateFormat1(new Date(monday))+' - '+this.Component.getDateFormat1(new Date( this.Component.getDateConverter(this.addDays(somedate, 6))))})
                monday = this.Component.getDateConverter(this.addDays(monday, 7))
            }
            weekData = JSON.stringify(inpWeek)
            //console.log(JSON.stringify(dataMapping));
            return view.render('content.sense.analytic.weeklyreport',{ dongleList, dongleId ,inpMonth, weekId, dataMapping, inpWeek, weekData, API_URL });
        }else{
            return view.render('content.sense.analytic.weeklyreport',{ dongleList, dongleId ,inpMonth, weekId, dataMapping, inpWeek, weekData, API_URL });
        }
    }

    getMonday(d) {
        d = new Date(d);
        var day = d.getDay(),
            diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
        return new Date(d.setDate(diff));
    }

    async analytic3({ session, view, request, response }){
        let dongleList = await this.Component.getSonicSensePartner(request.partner_id);
        let dongleId = null;
        let yearId = false;
        let dataMapping = new Array();
        let inpYear = null;
        if (request.post()._csrf) {
            let data = request.post();
            dongleId = data.build;
            inpYear = data.timetracking;
            dataMapping = JSON.stringify({ analytic: await this.Component.getMonthlyAnalytic(dongleId, inpYear)});
            //console.log(dataMapping)
            return view.render('content.sense.analytic.monthlyreport',{ dongleList, dongleId, yearId, dataMapping, inpYear });
        }else{
            return view.render('content.sense.analytic.monthlyreport',{ dongleList, dongleId, yearId, dataMapping, inpYear });
        }
    }

    async analytic7({ session,view, request, response }){
        let dongleList = await this.Component.getSonicSensePartner(request.partner_id);
        let dongleId = null;
        let realtime = false;
        let dataMapping = null;
        let distance = null;
        let timestep = 1;
        let size = 30;
        let inpStart = this.Component.getSenseAnalytic('start'); 
        let inpEnd = this.Component.getSenseAnalytic('end');
        if (request.post()._csrf) {
            let data = request.post();
            dongleId = data.build;
            distance = await Database.select('do_limit_distance')
                                         .from('sb_dongle')
                                         .where('scanner_chip_id', dongleId)
                                         .first();
            let sliderData = new Array(1,2,3,4,5,10,15,20,30);
            if(data.timestep){
                timestep = data.timestep;
            }else{
                timestep = 1;
            }
            
            if(data.size){
                size = data.size;
            }else{
                size = 30;
            }
            if(data.timetracking == null && data.timetracking2 == null){
                realtime = true;
            }
            inpStart = data.timetracking;
            inpEnd  = data.timetracking2;
            let dataAnalytic = await this.Component.getDeviceCountRealTime(dongleId, sliderData[timestep], size, inpStart, inpEnd, distance.do_limit_distance);
            dataMapping = JSON.stringify({ analytic: dataAnalytic });
            return view.render('content.sense.analytic.deviceaccountrealtime',{ inpStart, inpEnd, distance, timestep, size, realtime, dongleId, dataMapping, dongleList, APP_LIBRARY_UPLOAD, APP_ASSETS });
        }else{
            return view.render('content.sense.analytic.deviceaccountrealtime',{ inpStart, inpEnd, distance, timestep, size, realtime, dongleId, dataMapping, dongleList, APP_LIBRARY_UPLOAD, APP_ASSETS });
        }
    }

    addDays(dates, days){    
        var date = new Date(dates);
        date.setDate(date.getDate() + days);
        return date;
    }

    async analytic8({ session, view, request, response }){
        let dongleList = await this.Component.getSonicSensePartner(request.partner_id);
        let dongleId = null;
        let realtime = false;
        let dataMapping = null;
        let distance = null;
        let timestep = 1;
        let size = 30;
        let inpStart = this.Component.getSenseAnalytic('start'); 
        let inpEnd = this.Component.getSenseAnalytic('end');
        if (request.post()._csrf) {
            let data = request.post();
            dongleId = data.build;
            distance = await Database.select('do_limit_distance')
                                         .from('sb_dongle')
                                         .where('scanner_chip_id', dongleId)
                                         .first();
            let sliderData = new Array(1,2,3,4,5,10,15,20,30);
            if(data.timestep){
                timestep = data.timestep;
            }else{
                timestep = 1;
            }
            
            if(data.size){
                size = data.size;
            }else{
                size = 30;
            }
            if(data.timetracking == null && data.timetracking2 == null){
                realtime = true;
            }
            inpStart = data.timetracking;
            inpEnd  = data.timetracking2;
            let dataAnalytic = await this.Component.getDeviceCountRealTime(dongleId, sliderData[timestep], size, inpStart, inpEnd, distance.do_limit_distance);
            dataMapping = JSON.stringify({ analytic: dataAnalytic });
            return view.render('content.sense.analytic.devicerealtime',{ inpStart, inpEnd, distance, timestep, size, realtime, dongleId, dataMapping, dongleList, APP_LIBRARY_UPLOAD, APP_ASSETS });
        }else{
            return view.render('content.sense.analytic.devicerealtime',{ inpStart, inpEnd, distance, timestep, size, realtime, dongleId, dataMapping, dongleList, APP_LIBRARY_UPLOAD, APP_ASSETS });
        }
    }
    
    
}

module.exports = ContentController
