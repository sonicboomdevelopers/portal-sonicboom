'use strict'
const Database = use('Database');
const Hash = use('Hash');
const Env = use('Env');
const Component = use('./ComponentController');
const Encrypt = use('./EncryptController.js');
const User = use('App/Models/User');
class LoginController {
    constructor() {
        this.Component = new Component();
        this.Encrypt = new Encrypt();
    }
    async form({ session, auth, request, response, view }) {
        let data = request.post();
        let username, token;
        let warning = new Array();
        let redirect = false;
        const DatetimeNow = this.Component.getDateTimeNow();
        let queryString = this.Component.queryString(request.originalUrl());
        if (queryString.key !== undefined) {
            let checkPartner = await Database.table('sb_partner_sso_token').where('spst_token', queryString.key).first();
            if (checkPartner !== undefined) {
                if (auth.user !== null) {
                    let checkUserHasToken = await Database.table('sb_user_sso').where({ sus_u_id: auth.user.u_id, sus_spst_id: checkPartner.spst_id }).first();
                    if (checkUserHasToken === undefined) {
                        token = this.Encrypt.getSha256(Env.get('APP_KEY') + auth.user.u_id + checkPartner.spst_id)
                        await Database.insert({
                            sus_token: token,
                            sus_u_id: auth.user.u_id,
                            sus_spst_id: checkPartner.spst_id,
                            sus_access: DatetimeNow,
                            created_at: DatetimeNow
                        }).into('sb_user_sso');
                    } else {
                        token = checkUserHasToken.sus_token;
                        await Database.update({ sus_access: DatetimeNow }).where('sus_id', checkUserHasToken.sus_id).into('sb_user_sso');
                    }
                    redirect = true;
                } else {
                    if (request.post()._csrf) {
                        username = data.sonicboom_email;
                        try {
                            if (auth.user === null) {
                                let login = false;
                                const query = User.query();
                                query.innerJoin('sb_partner_admin', 'sb_partner_admin.pa_user_id', 'sb_user.u_id');
                                query.innerJoin('sb_partner', 'sb_partner.p_id', 'sb_partner_admin.pa_partner_id');
                                query.where({ u_is_active: 1, p_is_active: 1, u_username: data.sonicboom_email });
                                let userList = await query.fetch();
                                let UserDetail = await Database.table('sb_user').where('u_username', data.sonicboom_email).where('u_password', this.Component.hash(userList.rows[0].u_salt + data.sonicboom_password)).first();

                                if (UserDetail !== undefined) {
                                    let identifierUser = await User.find(UserDetail.u_id);
                                    if (userList.rows[0].pa_id === 0) {
                                        userList.rows[0].pa_privilege = 0;
                                    }
                                    try {
                                        await auth.login(identifierUser);
                                        session.put('branch_access', userList.rows[0].u_access_branch)
                                        session.put('user_partner', userList.rows[0].pa_partner_id)
                                        session.put('user_partner_type', userList.rows[0].p_type)
                                        session.put('user_privilege', userList.rows[0].pa_privilege)
                                        session.put('caid', userList.rows[0].p_app_id)
                                        login = true
                                    }
                                    catch (error) {
                                        console.log(error)
                                    }
                                }
                                if (login) {
                                    let checkUserHasToken = await Database.table('sb_user_sso').where({ sus_u_id: auth.user.u_id, sus_spst_id: checkPartner.spst_id }).first();
                                    if (checkUserHasToken === undefined) {
                                        token = this.Encrypt.getSha256(Env.get('APP_KEY') + auth.user.u_id + checkPartner.spst_id)
                                        await Database.insert({
                                            sus_token: token,
                                            sus_u_id: auth.user.u_id,
                                            sus_spst_id: checkPartner.spst_id,
                                            sus_access: DatetimeNow,
                                            created_at: DatetimeNow
                                        }).into('sb_user_sso');
                                    } else {
                                        token = checkUserHasToken.sus_token;
                                        await Database.update({ sus_access: DatetimeNow }).where('sus_id', checkUserHasToken.sus_id).into('sb_user_sso');
                                    }
                                    redirect = true
                                } else {
                                    console.log('gagal');
                                }
                            }
                        }
                        catch (error) {
                            console.log(error)
                            warning['level'] = 'danger';
                            warning['title'] = 'Username / Password is wrong.';
                        }
                    }
                }
                if (redirect) {
                    return response.redirect(checkPartner.spst_url + '?token=' + token);
                } else {
                    return view.render('master', { sonicboom_username: username, warning: warning });
                }
            } else {
                let error_code = Env.get('ERROR_STATUS_FORBIDDEN');
                let error_title = 'Key is not registerd';
                let error_description = "Sorry you'r key is not valid";
                return view.render('error', { error_code: error_code, error_title: error_title, error_description: error_description })
            }
        } else if (queryString.token !== undefined) {
            if (auth.user !== null) {
                let checkPartner = await Database.table('sb_partner_sso_token').where('spst_token', queryString.token).first();
                if (checkPartner) {
                    return response.redirect(checkPartner.spst_url + '?token=' + token);
                } else {
                    let error_code = Env.get('ERROR_STATUS_FORBIDDEN');
                    let error_title = 'Token not found';
                    let error_description = "Sorry your token is not found";
                    return view.render('error', { error_code: error_code, error_title: error_title, error_description: error_description })
                }
            } else {
                let error_code = Env.get('ERROR_STATUS_FORBIDDEN');
                let error_title = 'Forbidden';
                let error_description = "Sorry you key is not found";
                return view.render('error', { error_code: error_code, error_title: error_title, error_description: error_description })
            }
        } else {
            let error_code = Env.get('ERROR_STATUS_FORBIDDEN');
            let error_title = 'Forbidden';
            let error_description = "Sorry your don\'t have permission";
            return view.render('error', { error_code: error_code, error_title: error_title, error_description: error_description })
        }
    } 
async errors({ request, response, view }) {
    let error_code = Env.get('ERROR_STATUS_FORBIDDEN');
    let error_title = 'Forbidden';
    let error_description = 'Your access is denied';
    return view.render('error', { error_code: error_code, error_title: error_title, error_description: error_description })
}

/**
 * @apiDefine MyError
 * @apiError {Number} ok Status response.
 * @apiError {String} message Message response.
 */
/**
 * @api {get} /portal/?token=:token Implement feature Sonicboom in your iframe*
 * @apiName Set in your iframe
 * @apiGroup 2. IFRAME
 * @apiVersion 1.0.0
 *
 * @apiParam {String} token token one of data above.
 *
 * @apiSuccessExample Success showing feature:
 *     HTTP/1.1 200 OK
 *     {
 *       View page sonicboom feature.
 *     }
 *
 *  @apiErrorExample Error showing feature:
 *     HTTP/1.1 403 Not Found
 *     {
 *       View error page
 *     }
 */
async loginViaPortal({ auth, request, response, view }){
    let data = this.Component.decodeURLModular(request.originalUrl());
    if (data[1]) {
        let checkUserValidToken = await Database.table('sb_user_sso_token').where('sust_token', data[1]).first();
        if (checkUserValidToken !== undefined) {
            checkUserValidToken.sust_expired = this.Component.getTimeConverter(checkUserValidToken.sust_expired)
            if (this.Component.getTimeConverter(checkUserValidToken.sust_expired) >= this.Component.getDateTimeNow()) {
                await Database.table('sb_user_sso_token')
                    .where('sust_user_id', checkUserValidToken.sust_user_id)
                    .update({
                        sust_expired: this.Component.getSSOUserTimes(),
                    })
                if (auth.user) {
                    return response.redirect(data[0])
                } else {
                    let gettingUser = await User.find(checkUserValidToken.sust_user_id);
                    try {
                        await auth.login(gettingUser)
                        return response.redirect(data[0])
                    }
                    catch (error) {
                        console.log(error)
                    }
                    //valid = true
                }
            } else {
                let error_code = Env.get('ERROR_STATUS_FORBIDDEN');
                let error_title = 'Session Expired';
                let error_description = 'Sorry your session has been expired';
                return response.status(error_code).send(view.render('error', { error_code: error_code, error_title: error_title, error_description: error_description }))
            }
        } else {
            let error_code = Env.get('ERROR_DATA_NOT_FOUND');
            let error_title = 'Invalid token';
            let error_description = 'Sorry your token not found';
            return response.status(error_code).send(view.render('error', { error_code: error_code, error_title: error_title, error_description: error_description }))
        }
    } else {
        let error_code = Env.get('ERROR_STATUS_FORBIDDEN');
        let error_title = 'Bad Request';
        let error_description = 'Your token is missing';
        return response.status(error_code).send(view.render('error', { error_code: error_code, error_title: error_title, error_description: error_description }))
    }

}

}

module.exports = LoginController
