'use strict'
const Database = use('Database');
const Env = use('Env');
const Component = use('./../ComponentController');
const Encrypt = use('./..//EncryptController');
const User = use('App/Models/User');
class UserController {
    constructor () {
        this.Component = new Component();
        this.Encrypt = new Encrypt();
    }
    async checkUser({ request, response }){
        let dataRequest = request.post();
        const key = dataRequest.key;
        const user = dataRequest.user;
        let routeModular = '';
        if(key && user){
            const loginWithToken = await Database.select('sus_token as user_token', 'u_email as user_email', 'u_fullname as user_fullname', 'u_gender as user_gender', 'pa_partner_id as id_partner', 'sb_user.u_id as user')
                                                 .table('sb_user_sso')
                                                 .innerJoin('sb_user', 'sb_user_sso.sus_u_id', 'sb_user.u_id')
                                                 .innerJoin('sb_partner_admin', 'sb_partner_admin.pa_user_id', 'sb_user.u_id')
                                                 .innerJoin('sb_partner_key', 'sb_partner_key.k_partner', 'sb_partner_admin.pa_user_id')
                                                 .where({ k_key: key, u_username : user }).first();
            //console.log(loginWithToken);
            if(loginWithToken !== undefined){
                const getRoutePartner = await Database.select('srm_route', 'srm_title')
                                                    .table('sb_rule_modular')
                                                    .innerJoin('sb_partner_rule_modular', 'sb_partner_rule_modular.sprm_srm_id', 'sb_rule_modular.srm_id')
                                                    .where({ sprm_p_id: loginWithToken.id_partner});
                let timestamps = Math.round((new Date()).getTime() / 1000);
                let newToken = this.Encrypt.get_encrypt(Env.get('APP_KEY')+','+loginWithToken.user,timestamps)
                routeModular += '{'
                for(let i = 0; i < getRoutePartner.length; i++){
                    routeModular += '"'+getRoutePartner[i].srm_title+'":"'+encodeURIComponent(this.Encrypt.get_encrypt(getRoutePartner[i].srm_route+','+newToken,Env.get('KEY_MODULAR_FEATURE')))+'",';
                }
                routeModular = routeModular.substring(routeModular.length-1, 0)
                routeModular += '}';
                
                let saving = await Database.table('sb_user_sso_token')
                                       .where('sust_user_id', loginWithToken.user)
                                       .update({
                                            sust_token: newToken,
                                            sust_expired: this.Component.getSSOUserTimes(),
                                            sust_unixtimestamps: timestamps
                                        })
                if(saving){
                    response.send({ ok: 200, message: 'Success Get / Create / Update data', data : loginWithToken, route : JSON.parse(routeModular) });
                }else{
                    response.status(Env.get('ERROR_STATUS_SERVER_INTERNAL')).send({ok: Env.get('ERROR_STATUS_SERVER_INTERNAL'), message: 'Failed update.'})
                }
            }else{
                response.status(Env.get('ERROR_DATA_NOT_FOUND ')).send({ ok: Env.get('ERROR_DATA_NOT_FOUND '), message: 'Token cannot be found.'});
            }
        }else{
            response.status(Env.get('ERROR_STATUS_FORBIDDEN ')).send({ ok: Env.get('ERROR_STATUS_FORBIDDEN '), message: 'Forbidden.'});
        }
    }
    /**
     * @apiDefine MyError
     * @apiError {Number} ok Status response.
     * @apiError {String} message Message response.
     */
    /**
     * @api {post} /api/user/check_user User Authentication for get credentials
     * @apiName Authentication
     * @apiGroup 1. User
     * @apiVersion 1.0.0
     *
     * @apiParam {String} key Partner key from Sonicboom.
     * @apiParam {String} user Username login in my.sonicboom.co.id.
     *
     * @apiSuccess {Number} ok Status response.
     * @apiSuccess {String} message Message response.
     * @apiSuccess {Object} data  List of route if request is valid.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "ok": 200,
     *       "message": "Success Get/Update/Create.",
     *       "data": {
     *                  "analytic_7": "MXBweXVjbjR0eHFpaHdnbnRsMGNwZm2Cd2loMzksWHNLNE82bD5SRl9WZFdbVmQzTDlnT3NNXHE7MVF0MXxScU9jV0VfdFZueDdPQz9C",
     *                  "mapping": "MXBweXVjbjR0eHFpaHdnbnRsMGNwZm2Cd2loM29hcXdqcGkxWHVNMlI1bTZPOFdTZDdbWGYxTzhoVm1TWFpINlF2M3pVcFRZVDdXdFZzeDlRQUJB",
     *               }
     *     }
     *
     *  @apiUse MyError
     *
     *  @apiErrorExample Partner or User not be found:
     *     HTTP/1.1 403 Forbidden
     *     {
     *       "ok": 403,
     *       "message": "Partner or user not found."
     *     }
     *  @apiErrorExample {json} Update Error
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *       "ok": 500,
     *       "message": "Error internal."
     *     }
     */
    async login({ auth, request, response }) {
        const dataRequest = request.post()
        let routeModular = '';
        const checkPartnerKey = await Database.table('sb_partner_key')
                                              .innerJoin('sb_partner_admin', 'sb_partner_admin.pa_user_id', 'sb_partner_key.k_partner')
                                              .innerJoin('sb_user', 'sb_user.u_id', 'sb_partner_admin.pa_user_id')
                                              .where({ k_key: dataRequest.key});
        let userFound = false;
        let userId = false;
        let partnerId = false;
        for(let i = 0; i < checkPartnerKey.length; i++){
            if(checkPartnerKey[i].u_username == dataRequest.user || checkPartnerKey[i].u_email == dataRequest.user){
                userFound = true;
                userId = checkPartnerKey[i].u_id
                partnerId = checkPartnerKey[i].pa_partner_id
            }
        }
        if(userFound !== false){
            const getRoutePartner = await Database.select('srm_route', 'srm_title')
                                                  .table('sb_rule_modular')
                                                  .innerJoin('sb_partner_rule_modular', 'sb_partner_rule_modular.sprm_srm_id', 'sb_rule_modular.srm_id')
                                                  .where({ sprm_p_id: partnerId});
            let timestamps = Math.round((new Date()).getTime() / 1000);
            let newToken = this.Encrypt.get_encrypt(Env.get('APP_KEY')+','+userId,timestamps)
            routeModular += '{'
            for(let i = 0; i < getRoutePartner.length; i++){
                routeModular += '"'+getRoutePartner[i].srm_title+'":"'+encodeURIComponent(this.Encrypt.get_encrypt(getRoutePartner[i].srm_route+','+newToken,Env.get('KEY_MODULAR_FEATURE')))+'",';
            }
            routeModular = routeModular.substring(routeModular.length-1, 0)
            routeModular += '}';
            let Saving = await Database.table('sb_user_sso_token')
                                       .where('sust_user_id', userId)
                                       .update({
                                            sust_token: newToken,
                                            sust_expired: this.Component.getSSOUserTimes(),
                                            sust_unixtimestamps: timestamps
                                        })
                            
            if (Saving) {
                response.status(200).send({ ok: 200, message: 'Success Get/Update/Create.', data: JSON.parse(routeModular) });
            } else {
                response.status(Env.get('ERROR_STATUS_SERVER_INTERNAL')).send({ok: Env.get('ERROR_STATUS_SERVER_INTERNAL'), message: 'Error internal server.'});
            }
        }else{
            response.status(Env.get('ERROR_DATA_NOT_FOUND ')).send({ ok: Env.get('ERROR_DATA_NOT_FOUND '), message: 'Partner or user not found.' });
        }
        
    }
}

module.exports = UserController
