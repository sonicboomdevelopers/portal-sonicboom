'use strict'
const Database = use('Database');
const Env = use('Env');
const Component = use('./../ComponentController');
class ApiAjaxSenseController {
    constructor () {
        this.Component = new Component();
    }
    async getRealTime({ request, response }){
        let dataRequest = request.post();
        let dongleId = dataRequest.ss;
        let step = dataRequest.step;
        let size = dataRequest.size;
        let inpStart = dataRequest.start;
        let inpEnd = dataRequest.end;
        let distance = dataRequest.distance;
        response.send(await this.Component.getDeviceCountRealTime(dongleId, step, size, inpStart, inpEnd, distance));
    }

    async getDeviceRealTime({ request, response }){
        let dataRequest = request.post();
        let dongleId = dataRequest.ss;
        let size = dataRequest.size;
        let inpStart = dataRequest.start;
        let inpEnd = dataRequest.end;
        let distance = dataRequest.distance;
        response.send(await this.Component.getDeviceRealTime(dongleId, size,'', inpStart, inpEnd, distance));
    }

    async getWeek({ request, response }){
        let dataRequest = request.post();
        let month = dataRequest.month;
        let week = new Array();
        let monday = this.Component.getFirstMondayOfMonth(new Date(month));
        for(let i=0; i < 5; i++){
            let somedate = new Date(String(monday));
            week.push({ key :  monday+'|'+ this.Component.getDateConverter(this.addDays(somedate, 6)), value: this.Component.getDateFormat1(new Date(monday))+' - '+this.Component.getDateFormat1(new Date( this.Component.getDateConverter(this.addDays(somedate, 6))))});
            monday = this.Component.getDateConverter(this.addDays(monday, 7));
        }
        response.send(week);
    }

    async getFloor({ session, request, response }){
        let data = request.post();
        let buildingId = data.build_id;
        let floor = await this.Component.getDongleFloor(session.get('user_partner'), buildingId);
        response.send(floor);
    }

    async mappingRealTime({session, request, response }){
        let data = request.post();
        let floorId = data.floor;
        let list = await this.Component.getDongleMapping(floorId);
        response.send(list);
    }

    async mappingRealTimeDevices({session, request, response }){
        let data = request.post();
        let floorId = data.floor;
        let list = await this.Component.getDongleMappingDevice(floorId);
        response.send(list);
    }

    addDays(dates, days){    
        var date = new Date(dates);
        date.setDate(date.getDate() + days);
        return date;
    }
}

module.exports = ApiAjaxSenseController
