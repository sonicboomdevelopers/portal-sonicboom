'use strict'
const Database = use('Database');
const Hash = use('Hash');
const Env = use('Env');
const APP_LIBRARY_UPLOAD = Env.get('APP_LIBRARY_UPLOAD');
const APP_ASSETS = Env.get('APP_ASSETS');
const Component = use('./ComponentController');
const Encrypt = use('./EncryptController.js');
const User = use('App/Models/User');
class ContentController {
    constructor() {
        this.Component = new Component();
        this.Encrypt = new Encrypt();
    }
    async dashboard({ session, auth, request, response, view }){
        return view.render('content.dashboard',{ });
    }

    async profileView({ request, session, view, params }){
        let partnerDetail = await Database.from('sb_partner')
                                      .where('p_id', request.partner_id)
                                      .first();
        let getGenerateKey = await Database.select('k_key')
                                           .from('sb_partner_key')
                                           .where('k_partner', request.partner_id)
                                           .first();
        let legal = partnerDetail.p_legal_name;
        let short = partnerDetail.p_short_name;
        let appId = partnerDetail.p_app_id;
        let companyId = partnerDetail.p_company_id;
        let apiKey = getGenerateKey.k_key;
        let logo = getGenerateKey.p_logo_url;
        let messageLimit = partnerDetail.p_message_counter_limit;
        let city = partnerDetail.p_city;
        let postCode = partnerDetail.p_postal_code;
        let phone = partnerDetail.p_phone;
        let address = partnerDetail.p_address_1;
        let alterAddres = partnerDetail.p_address_2;
        return view.render('content.profile.view',{ legal, short, appId, companyId, apiKey, logo, messageLimit, city, postCode, phone, address, alterAddres, APP_LIBRARY_UPLOAD, APP_ASSETS });
    }
}

module.exports = ContentController
