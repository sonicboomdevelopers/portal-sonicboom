'use strict'

const BaseExceptionHandler = use('BaseExceptionHandler')
const Env = use('Env');
/**
 * This class handles all exceptions thrown during
 * the HTTP request lifecycle.
 *
 * @class ExceptionHandler
 */
class ExceptionHandler extends BaseExceptionHandler {
  /**
   * Handle exception thrown during the HTTP lifecycle
   *
   * @method handle
   *
   * @param  {Object} error
   * @param  {Object} options.request
   * @param  {Object} options.response
   *
   * @return {void}
   */
  async handle (error, { request, response, view }) {
    if(request.method() === 'POST'){
      response.status(error.status).send(error.message);
    }else{
      let error_code = error.status;
      let error_title = 'Error';
      let error_description = ''
      if(error.status === parseInt(Env.get('ERROR_STATUS_SERVER_INTERNAL'))){
        error_description = 'Internal Error';
      }else if(error.status === parseInt(Env.get('ERROR_STATUS_PAGE_NOT_FOUND'))){
        error_title = 'Page Not Found';
        error_description = 'Sorry page not found';
      }else if(error.status === parseInt(Env.get('ERROR_STATUS_FORBIDDEN'))){
        error_title = 'Forbidden';
      }
      else{
        error_description = error.message;
      }
      return response.status(error.status).send(error.message)
      //return response.send(view.render('error', { error_code: error_code, error_title:error_title, error_description: error_description }))
    }
  }

  /**
   * Report exception for logging or debugging.
   *
   * @method report
   *
   * @param  {Object} error
   * @param  {Object} options.request
   *
   * @return {void}
   */
  async report (error, { request }) {
  }
}

module.exports = ExceptionHandler
